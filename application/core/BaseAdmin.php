<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaseAdmin extends BaseController {

	function __construct()
	{
		parent:: __construct();
	}

	public function template($page, $data = false)
	{
		$this->load->view('template/admin/header', $data);
		$this->load->view('template/admin/sidebar', $data);
		$this->load->view($page, $data);
		$this->load->view('template/admin/footer', $data);
	}
}
