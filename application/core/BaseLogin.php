<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaseLogin extends BaseController {

	function __construct()
	{
		parent:: __construct();
	}

	public function template($page, $data = false)
	{
		$this->load->view('template/login/header', $data);
		$this->load->view($page, $data);
		$this->load->view('template/login/footer', $data);
	}
}
