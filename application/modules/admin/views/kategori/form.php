    <div class="x_panel">
      <div class="x_title">
        <h2>Input Kategori<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>

      <div class="form-group">
        <!-- start form for validation -->
        <form id="demo-form" action="<?= site_url('admin/kategori/save'); ?>" method="POST" data-parsley-validate>
          <label for="fullname">Nama Kategori * :</label>
          <input type="text" class="form-control" name="nama_kategori" />
          
          <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-4">
              <button class="btn btn-primary" type="button">Cancel</button>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

        </form>
        <!-- end form for validations -->
      </div>
    </div>