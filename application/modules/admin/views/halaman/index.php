  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <!-- <h2>Slider</h2> -->
        <a class="btn btn-success btn-sm pull-left" href="<?= site_url('admin/halaman/add/'); ?>"><i class="fa fa-plus"></i> Tambah Halaman</a>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-fixed-header" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Judul</th>
              <th>Tanggal Update</th>
              <th>Tanggal Publis</th>
              <th>Public</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
          	<?php
          	foreach($data as $data):
          	?>
            <tr>
              <td><?= $data['judul_hal']; ?></td>
              <td><?= $data['tgl_update_hal']; ?></td>
              <td><?= $data['tgl_publis_hal']; ?></td>
              <td><?= $data['publis_hal']; ?></td>
              <td>
                <a href="<?= site_url('admin/halaman/delete/'.$data['id_halaman']); ?>"><i><span class="glyphicon glyphicon-trash"></span></i></a>
              </td>
            </tr>
            <?php
        	endforeach;
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>