<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		// $data = $this->db->query('SELECT * FROM artikel')->result_array();
		$data = $this->db->query("SELECT * FROM tag")->result_array();

		$this->template('tag/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('tag', array('id_tag' => $id))->row_array();

		$this->template('tag/form', $data);
	}

	public function save()
	{
		$data = array(
			'tag' => $this->input->post('tag')
		);
		$this->db->insert('tag', $data);
		redirect('admin/tag');
	}

	public function delete($id)
	{
		$where = array('id_tag' => $id);
		$data = $this->db->delete('tag', $where);

		if($data >= 1){
			echo "Berhasil Menghapus Data";
		}
		redirect('admin/tag');
	}
}
