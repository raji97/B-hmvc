<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		$this->template('home/index');
	}

}
