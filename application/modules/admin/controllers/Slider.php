<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		$data = $this->db->query('SELECT * FROM tbl_slider')->result_array();

		$this->template('slider/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('tbl_slider', array('id_slider' => $id))->row_array();

		$this->template('slider/form', $data);
	}

	public function save()
	{
		
        $config['upload_path']          = './uploads/slider';
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx';
        
        

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('filename'))
        {
            $upload = $this->upload->data();

            $data = array(
            	'filename' => $upload['file_name'],
            	'title' => $this->input->post('title'),
				'description' => $this->input->post('description')
            );

            $this->db->insert('tbl_slider', $data);

            redirect('admin/slider');
        }
        else
        {
            $upload = $this->upload->data();

            $data = array(
            	'filename' => $upload['file_name'],
            	'title' => $this->input->post('title'),
				'description' => $this->input->post('description')
            );
            $this->db->insert('tbl_slider', $data);

            redirect('admin/slider');
        }	
	}

	public function delete($id)
	{
		$where = array('id_slider' => $id);
		// $data = $this->M_crud->hapus('tbl_anggota', $where);
		$data = $this->db->delete('tbl_slider', $where);

		if($data>=1){
			echo "Berhasil menghapus data";
		}

		redirect('admin/slider');
	}	
}
