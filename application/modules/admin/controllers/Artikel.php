<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		// $data = $this->db->query('SELECT * FROM artikel')->result_array();
		$data = $this->db->query("SELECT a.id_artikel, a.judul, date_format(a.tgl_update, '%d-%m-%Y %H:%i:%s') as tgl_update,  date_format(a.tgl_publis, 					'%d-%m-%Y %H:%i:%s' ) as tgl_publis, a.publis 
								FROM artikel as a", false)->result_array();

		$this->template('artikel/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('artikel', array('id_artikel' => $id))->row_array();

		$data ['kategori'] = $this->db->get_where('kategori')->result_array();
		$data ['tag'] = $this->db->get_where('tag')->result_array();

		$this->template('artikel/form', $data);
	}

	public function save()
	{
		$id_kategori = $this->input->post('id_kategori');
		$id_tag = $this->input->post('id_tag');
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$publis = $this->input->post('publis');
		
		$tgl_p = $this->input->post('tgl_publis');
		$tgl_p = str_replace('/', '-', $tgl_p);
		$tgl_publis = date('Y-m-d H:i:s', strtotime($tgl_p));

		$tgl_u = $this->input->post('tgl_update');
		$tgl_u =str_replace('/', '-', $tgl_u);
		$tgl_update = date('Y-m-d H:i:s', strtotime($tgl_u));

		$data = array(
			'id_kategori' => $id_kategori,
			'id_tag' => $id_tag,
			'judul' => $judul,
			'isi' => $isi,
			'publis' => $publis,
			'tgl_publis' => $tgl_publis,
			'tgl_update' => $tgl_update
		);
		$this->db->insert('artikel', $data);
		redirect('admin/artikel');
	}

	public function delete($id)
	{
		$where = array('id_artikel' => $id);
		$data = $this->db->delete('artikel', $where);

		if($data >= 1){
			echo "Berhasil Menghapus Data";
		}
		redirect('admin/artikel');
	}
}
