<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		$data = $this->db->query("SELECT h.id_halaman, h.judul_hal, date_format(h.tgl_publis_hal, '%d-%m-%Y %H:%i:%s') as tgl_publis_hal, h.publis_hal,
						date_format(h.tgl_update_hal, '%d-%m-%Y %H:%i:%s') as tgl_update_hal
						FROM halaman as h", false)->result_array();

		$this->template('halaman/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('halaman', array('id_halaman' => $id))->row_array();
		$this->template('halaman/form', $data);
	}

	public function save()
	{
		$judul_hal = $this->input->post('judul_hal');
		$isi_hal = $this->input->post('isi_hal');

		$tgl_p = $this->input->post('tgl_publis_hal');
		$tgl_p = str_replace('/','-',$tgl_p);
		$tgl_publis_hal = date('Y-m-d H-i-s', strtotime($tgl_p));

		$tgl_u = $this->input->post('tgl_update_hal');
		$tgl_u = str_replace('/', '-', $tgl_u);
		$tgl_update_hal = date('Y-m-d H:i:s', strtotime($tgl_u));
		$publis_hal = $this->input->post('publis_hal');

		$data = array(
			'judul_hal' => $judul_hal,
			'isi_hal' => $isi_hal,
			'tgl_publis_hal' => $tgl_publis_hal,
			'tgl_update_hal' => $tgl_update_hal,
			'publis_hal' => $publis_hal,
		);
		$this->db->insert('halaman', $data);
		redirect('admin/halaman');
	}

	public function delete($id)
	{
		$where = array('id_halaman' => $id);
		$data = $this->db->delete('halaman', $where);

		if($data >= 1){
			echo "Berhasil Menghapus Data";
		}
		redirect('admin/halaman');
	}
}