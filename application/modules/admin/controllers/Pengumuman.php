<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		$data = $this->db->query("SELECT p.id_peng, p.judul_peng, date_format(p.tgl_update_peng, '%d-%m-%Y %H:%i:%s') as tgl_update_peng,
								date_format(p.tgl_publis_peng,'%d-%m-%Y %H:%i:%s' ) as tgl_publis_peng, p.publis_peng 
								FROM pengumuman as p", false)->result_array();

		$this->template('pengumuman/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('pengumuman', array('id_peng' => $id))->row_array();

		$this->template('pengumuman/form', $data);
	}

	public function save()
	{
		$judul_peng = $this->input->post('judul_peng');
		$isi_peng = $this->input->post('isi_peng');
		$publis_peng = $this->input->post('publis_peng');
		$tgl_p = $this->input->post('tgl_publis_peng');
		$tgl_p = str_replace('/', '-', $tgl_p);
		$tgl_publis_peng = date('Y-m-d H:i:s', strtotime($tgl_p));

		$tgl_u = $this->input->post('tgl_update_peng');
		$tgl_u =str_replace('/', '-', $tgl_u);
		$tgl_update_peng = date('Y-m-d H:i:s', strtotime($tgl_u));

		$data = array(
			'judul_peng' => $judul_peng,
			'isi_peng' => $isi_peng,
			'publis_peng' => $publis_peng,
			'tgl_publis_peng' => $tgl_publis_peng,
			'tgl_update_peng' => $tgl_update_peng,
		);
		$this->db->insert('pengumuman', $data);
		redirect('admin/pengumuman');
	}

	public function delete($id)
	{
		$where = array('id_peng' => $id);
		$data = $this->db->delete('pengumuman', $where);

		if($data >= 1){
			echo "Berhasil Menghapus Data";
		}
		redirect('admin/pengumuman');
	}
}
