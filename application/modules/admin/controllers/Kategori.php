<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends BaseAdmin {

	function __construct()
	{
		parent:: __construct();
		if(! $this->session->username){
			redirect('login');
		}
	}

	public function index()
	{
		// $data = $this->db->query('SELECT * FROM artikel')->result_array();
		$data = $this->db->query("SELECT * FROM kategori")->result_array();

		$this->template('kategori/index', array('data' => $data));
	}

	public function add($id='')
	{
		$data = $this->db->get_where('kategori', array('id_kategori' => $id))->row_array();

		$this->template('kategori/form', $data);
	}

	public function save()
	{
		$data = array(
			'nama_kategori' => $this->input->post('nama_kategori'),
		);
		$this->db->insert('kategori', $data);
		redirect('admin/kategori');
	}

	public function delete($id)
	{
		$where = array('id_kategori' => $id);
		$data = $this->db->delete('kategori', $where);

		if($data >= 1){
			echo "Berhasil Menghapus Data";
		}
		redirect('admin/kategori');
	}
}
