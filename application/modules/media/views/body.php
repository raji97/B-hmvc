<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/img_bg_4.jpg)" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row row-mt-15em">
				<div class="col-md-12 mt-text text-center animate-box" data-animate-effect="fadeInUp">
					<h1>We Build <strong>Branded Platforms</strong></h1>	
					<h2>Far far away, behind the word mountains, far from the countries Vokalia.</h2>
					<div class="text-center"><a href="https://vimeo.com/channels/staffpicks/93951774" class="btn btn-primary btn-lg popup-vimeo">Watch the video</a></div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="overflow-hid"> 
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-12 col-md-offset-2 text-left gtco-heading">
						<h4>Pengumuman</h4>
						<?php $i=0; foreach($pengumuman as $data) if(++$i < 2) {?>
							<h3><?= $data['judul_peng']; ?></h3>
							<p><?= $data['isi_peng']; ?></p>
							<hr />
						<?php }?>
					</div>
				</div>
				<div class="row">
					<?php
					foreach($artikel as $data) 
					{?>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<a href="#" class="gtco-card-item">
							<figure>
								<div class="overlay"><i class="ti-plus"></i></div>
								<img src="images/img_1.jpg" alt="Image" class="img-responsive">
							</figure>
							<div class="gtco-text">
								<h2><?= $data['judul']; ?></h2>
								<p><?= $data['isi']; ?></p>
								<p><span class="btn btn-primary">Learn more</span></p>
							</div>
						</a>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>


	<div class="gtco-cover gtco-cover-sm" style="background-image: url(images/img_bg_1.jpg)"  data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="gtco-container text-center">
			<div class="display-t">
				<div class="display-tc">
					<h1>We have high quality services that you will surely love!</h1>
				</div>	
			</div>
		</div>
	</div>

	