<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends BaseUtama {

	function __construct()
	{
		parent:: __construct();
	}

	public function index()
	{
		$data ['pengumuman'] = $this->db->get('pengumuman')->result_array();
		$data ['artikel'] = $this->db->get('artikel')->result_array();
		$this->template('body', $data);
	}

}
