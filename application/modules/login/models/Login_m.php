<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends CI_Model {

	public function login($user, $pass){

		$this->db->select('username, password');
		$this->db->from('user');
		$this->db->where('username', $user);
		$this->db->where('password', md5($pass));
		// $this->db->limit(1);

		$data = $this->db->get();

		if($data->num_rows() == 1 )
		{
			return $data->result();
		}else{
			return false;
		}

	}
}
