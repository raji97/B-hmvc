<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends BaseLogin {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_m');
	}

	public function index()
	{
		$this->template('form');
	}

	public function proses_login(){

		$user = $this->input->post('username');
		$pass = $this->input->post('password');

		$cek = $this->login_m->login($user, $pass);

		if($cek){
			// foreach ($cek as $c);

			$this->session->set_userdata('username', $user);

			redirect('admin/home');

		}else{
			
			redirect('login');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
	
}
