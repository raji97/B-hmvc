-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.28-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for webstmik
CREATE DATABASE IF NOT EXISTS `webstmik` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `webstmik`;

-- Dumping structure for table webstmik.artikel
CREATE TABLE IF NOT EXISTS `artikel` (
  `id_artikel` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) DEFAULT NULL,
  `id_tag` int(11) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `isi` longtext,
  `tgl_publis` datetime DEFAULT CURRENT_TIMESTAMP,
  `publis` tinyint(2) DEFAULT NULL COMMENT '1 publis 0 tidak',
  `tgl_update` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_artikel`),
  KEY `FK_artikel_kategori` (`id_kategori`),
  KEY `FK_artikel_tag` (`id_tag`),
  CONSTRAINT `FK_artikel_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_artikel_tag` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id_tag`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.artikel: ~0 rows (approximately)
/*!40000 ALTER TABLE `artikel` DISABLE KEYS */;
INSERT INTO `artikel` (`id_artikel`, `id_kategori`, `id_tag`, `judul`, `isi`, `tgl_publis`, `publis`, `tgl_update`) VALUES
	(79, NULL, NULL, 'STMIK', 'STIMIK tempat belajar yang bagus', '2018-05-06 18:18:21', 1, '2018-05-06 18:18:21');
/*!40000 ALTER TABLE `artikel` ENABLE KEYS */;

-- Dumping structure for table webstmik.halaman
CREATE TABLE IF NOT EXISTS `halaman` (
  `id_halaman` int(10) NOT NULL AUTO_INCREMENT,
  `judul_hal` varchar(50) DEFAULT NULL,
  `isi_hal` longtext,
  `tgl_publis_hal` datetime DEFAULT CURRENT_TIMESTAMP,
  `publis_hal` tinyint(2) DEFAULT NULL COMMENT '1 publis 0 tidak',
  `tgl_update_hal` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_halaman`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.halaman: ~2 rows (approximately)
/*!40000 ALTER TABLE `halaman` DISABLE KEYS */;
INSERT INTO `halaman` (`id_halaman`, `judul_hal`, `isi_hal`, `tgl_publis_hal`, `publis_hal`, `tgl_update_hal`) VALUES
	(0, 'STMIK', NULL, '2018-05-07 12:59:20', 1, '2018-05-07 12:59:26'),
	(8, '', '', '2018-05-08 18:14:48', 1, '2018-05-08 18:14:49');
/*!40000 ALTER TABLE `halaman` ENABLE KEYS */;

-- Dumping structure for table webstmik.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.kategori: ~0 rows (approximately)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table webstmik.pengumuman
CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id_peng` int(11) NOT NULL AUTO_INCREMENT,
  `judul_peng` varchar(50) DEFAULT NULL,
  `isi_peng` longtext,
  `tgl_publis_peng` datetime DEFAULT CURRENT_TIMESTAMP,
  `publis_peng` tinyint(4) DEFAULT NULL COMMENT '1 publis 0 tidak',
  `tgl_update_peng` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_peng`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.pengumuman: ~4 rows (approximately)
/*!40000 ALTER TABLE `pengumuman` DISABLE KEYS */;
INSERT INTO `pengumuman` (`id_peng`, `judul_peng`, `isi_peng`, `tgl_publis_peng`, `publis_peng`, `tgl_update_peng`) VALUES
	(1, 'STMIK', 'bukak lowongan baru', '2018-05-12 15:11:50', 1, '2018-05-12 15:11:55'),
	(2, 'hhhh', 'ggg', '2018-05-12 15:30:05', 0, '2018-05-12 15:30:07'),
	(5, 'h', '', '2018-05-12 16:39:51', 1, '2018-05-12 16:39:52');
/*!40000 ALTER TABLE `pengumuman` ENABLE KEYS */;

-- Dumping structure for table webstmik.tag
CREATE TABLE IF NOT EXISTS `tag` (
  `id_tag` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.tag: ~0 rows (approximately)
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;

-- Dumping structure for table webstmik.tbl_slider
CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id_slider` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_slider`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.tbl_slider: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_slider` DISABLE KEYS */;
INSERT INTO `tbl_slider` (`id_slider`, `filename`, `title`, `description`) VALUES
	(40, 'stmik1.jpg', 'Stimik', 'Abulyatama'),
	(41, 'stmik.jpg', 'Dekan', 'Stmik');
/*!40000 ALTER TABLE `tbl_slider` ENABLE KEYS */;

-- Dumping structure for table webstmik.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table webstmik.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
